package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/dustin/go-humanize"

	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
)

var (

	// CloudID Endpoint for the Elastic Service (https://elastic.co/cloud)
	CloudID = "foo"

	// UserName cluster elastic cloud
	UserName = "foo"

	// Password to es cluster user
	Password = "foo"

	// ESClient elastic search client
	esClient *elasticsearch.Client

	_               = fmt.Print
	indexName       = "test-bulk-example"
	numWorkers      = runtime.NumCPU()
	flushBytes      = 1e+6
	countSuccessful uint64
	count           = 100000
	batch           = 25000

	buf bytes.Buffer
	res *esapi.Response
	err error
	raw map[string]interface{}
	blk *bulkResponse

	articles []*Article

	numItems   int
	numErrors  int
	numIndexed int
	numBatches int
	currBatch  int
)

// Article example to indexer
type Article struct {
	ID        int       `json:"id"`
	Title     string    `json:"title"`
	Body      string    `json:"body"`
	Published time.Time `json:"published"`
	Author    Author    `json:"author"`
}

// Author article owner
type Author struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

type bulkResponse struct {
	Errors bool `json:"errors"`
	Items  []struct {
		Index struct {
			ID     string `json:"_id"`
			Result string `json:"result"`
			Status int    `json:"status"`
			Error  struct {
				Type   string `json:"type"`
				Reason string `json:"reason"`
				Cause  struct {
					Type   string `json:"type"`
					Reason string `json:"reason"`
				} `json:"caused_by"`
			} `json:"error"`
		} `json:"index"`
	} `json:"items"`
}

func newClient() (*elasticsearch.Client, error) {

	// Config represents the client configuration
	cfg := elasticsearch.Config{
		CloudID:  CloudID,
		Username: UserName,
		Password: Password,
	}

	return elasticsearch.NewClient(cfg)

}

// generateArticles collection
func generateArticles() []*Article {
	//
	var articles []*Article

	names := []string{"Plato", "Ugenio", "Ecludes", "Enzo"}
	lastNames := []string{"Alonso", "Urso", "Mattioli", "Rossi"}

	for i := 1; i <= count; i++ {
		articles = append(articles, &Article{
			ID:        i,
			Title:     strings.Join([]string{"Title", strconv.Itoa(i)}, " "),
			Body:      "Lorem ipsum dolor sit amet...",
			Published: time.Now().Round(time.Second).UTC().AddDate(0, 0, i),
			Author: Author{
				FirstName: names[rand.Intn(len(names))],
				LastName:  lastNames[rand.Intn(len(lastNames))],
			},
		})
	}

	log.Printf("→ Generated %s articles", humanize.Comma(int64(len(articles))))
	return articles
}

func recreateIndex() error {

	res, err = esClient.Indices.Delete([]string{indexName}, esClient.Indices.Delete.WithIgnoreUnavailable(true))
	if err != nil || res.IsError() {
		return err
	}
	res.Body.Close()
	log.Printf("→ Deleted %s index", indexName)

	res, err = esClient.Indices.Create(indexName)
	if err != nil {
		return err
	}
	res.Body.Close()

	log.Printf("→ Re-created %s index", indexName)
	return nil
}

func printStats(since time.Time) {

	dur := time.Since(since)

	if numErrors > 0 {
		log.Fatalf(
			"Indexed [%s] documents with [%s] errors in %s (%s docs/sec)",
			humanize.Comma(int64(numIndexed)),
			humanize.Comma(int64(numErrors)),
			dur.Truncate(time.Millisecond),
			humanize.Comma(int64(1000.0/float64(dur/time.Millisecond)*float64(numIndexed))),
		)
	} else {
		log.Printf(
			"Sucessfuly indexed [%s] documents in %s (%s docs/sec)",
			humanize.Comma(int64(numIndexed)),
			dur.Truncate(time.Millisecond),
			humanize.Comma(int64(1000.0/float64(dur/time.Millisecond)*float64(numIndexed))),
		)
	}
}

func errBulkRequest(statusCode int, body io.ReadCloser) {

	numErrors += numItems

	if err := json.NewDecoder(body).Decode(&raw); err != nil {
		log.Fatalf("Failure to to parse response body: %s", err)
	}

	log.Printf("  Error: [%d] %s: %s",
		statusCode,
		raw["error"].(map[string]interface{})["type"],
		raw["error"].(map[string]interface{})["reason"],
	)

}

func readResponse(body io.ReadCloser) {

	if err := json.NewDecoder(body).Decode(&blk); err != nil {
		log.Fatalf("Failure to to parse response body: %s", err)
	}

	for _, d := range blk.Items {

		// HTTP status above 201
		if d.Index.Status > 201 {

			// increment the error counter
			numErrors++

			// print
			log.Printf("  Error: [%d]: %s: %s: %s: %s",
				d.Index.Status,
				d.Index.Error.Type,
				d.Index.Error.Reason,
				d.Index.Error.Cause.Type,
				d.Index.Error.Cause.Reason,
			)
		}

		numIndexed++

	}

}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	// log.Println(elasticsearch.Version)
	defaultIndexer()
}

// defaultIndexer elasticsearch's Bulk API.
func defaultIndexer() {
	log.SetFlags(0)

	log.Printf(
		"\x1b[1mBulk API\x1b[0m: documents [%s] batch size [%s]",
		humanize.Comma(int64(count)), humanize.Comma(int64(batch)))
	log.Println(strings.Repeat("▁", 65))

	// Create the Elasticsearch client
	//
	esClient, err = newClient()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	// Generate the articles collection
	articles := generateArticles()

	// Re-create the index
	err = recreateIndex()
	if err != nil {
		log.Fatalf("Cannot re-create index: %s", res)
	}

	if count%batch == 0 {
		numBatches = (count / batch)
	} else {
		numBatches = (count / batch) + 1
	}

	start := time.Now().UTC()

	// Loop over the collection
	//
	for i, a := range articles {
		numItems++

		currBatch = i / batch
		if i == count-1 {
			currBatch++
		}

		// Prepare the metadata payload
		meta := []byte(fmt.Sprintf(`{ "index" : { "_id" : "%d" } }%s`, a.ID, "\n"))

		// Prepare the data payload: encode article to JSON
		data, err := json.Marshal(a)
		if err != nil {
			log.Fatalf("Cannot encode article %d: %s", a.ID, err)
		}

		// Append newline to the data payload
		data = append(data, "\n"...) // <-- Comment out to trigger failure for batch

		// Append payloads to the buffer (ignoring write errors)
		buf.Grow(len(meta) + len(data))
		_, _ = buf.Write(meta)
		_, _ = buf.Write(data)

		// When a threshold is reached, execute the Bulk() request with body from buffer
		if i > 0 && i%batch == 0 || i == count-1 {
			fmt.Print("→ Sending batch ")
			fmt.Printf("[%d/%d] \n", currBatch, numBatches)

			res, err = esClient.Bulk(bytes.NewReader(buf.Bytes()), esClient.Bulk.WithIndex(indexName))
			if err != nil {
				log.Fatalf("Failure indexing batch %d: %s", currBatch, err)
			}

			// If the whole request failed, print error and mark all documents as failed
			if res.IsError() {
				errBulkRequest(res.StatusCode, res.Body)
			}

			// A successful response might still contain errors for particular documents...
			readResponse(res.Body)

			// Close the response body
			res.Body.Close()

			// Reset the buffer and items counter
			//
			buf.Reset()
			numItems = 0
		}
	}

	// Report the results: number of indexed docs, number of errors, duration, indexing rate
	fmt.Print("\n")
	log.Println(strings.Repeat("▔", 65))
	printStats(start)

}
