package main

import (
	"bytes"
	"context"
	"encoding/json"
	"log"
	"math/rand"
	"runtime"
	"strconv"
	"strings"
	"sync/atomic"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/dustin/go-humanize"

	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
	"github.com/elastic/go-elasticsearch/v7/esutil"
)

var (
	indexName  = "test-bulk-example"
	numWorkers = runtime.NumCPU()
	flushBytes = 1e+6
	numItems   = 100000

	countSuccessful uint64

	res *esapi.Response
	err error

	// CloudID Endpoint for the Elastic Service (https://elastic.co/cloud)
	CloudID = "foo"

	// UserName cluster elastic cloud
	UserName = "foo"

	// Password to es cluster user
	Password = "foo"

	// esClient elastic search client
	esClient *elasticsearch.Client
)

// Article example to indexer
type Article struct {
	ID        int       `json:"id"`
	Title     string    `json:"title"`
	Body      string    `json:"body"`
	Published time.Time `json:"published"`
	Author    Author    `json:"author"`
}

// Author article owner
type Author struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	// log.Println(elasticsearch.Version)
	ParallelIndexer()
}

// generateArticles collection
func generateArticles() []*Article {
	//
	var articles []*Article

	names := []string{"Plato", "Ugenio", "Ecludes", "Enzo"}
	lastNames := []string{"Alonso", "Urso", "Mattioli", "Rossi"}

	for i := 1; i <= numItems; i++ {
		articles = append(articles, &Article{
			ID:        i,
			Title:     strings.Join([]string{"Title", strconv.Itoa(i)}, " "),
			Body:      "Lorem ipsum dolor sit amet...",
			Published: time.Now().Round(time.Second).UTC().AddDate(0, 0, i),
			Author: Author{
				FirstName: names[rand.Intn(len(names))],
				LastName:  lastNames[rand.Intn(len(lastNames))],
			},
		})
	}

	log.Printf("→ Generated %s articles", humanize.Comma(int64(len(articles))))
	return articles
}

func newClient() (*elasticsearch.Client, error) {

	// Use a third-party package for implementing the backoff function
	retryBackoff := backoff.NewExponentialBackOff()

	// Config represents the client configuration
	cfg := elasticsearch.Config{
		CloudID:  CloudID,
		Username: UserName,
		Password: Password,

		// Retry on 429 TooManyRequests statuses
		RetryOnStatus: []int{502, 503, 504, 429},

		// Configure the backoff function
		RetryBackoff: func(i int) time.Duration {
			if i == 1 {
				retryBackoff.Reset()
			}
			return retryBackoff.NextBackOff()
		},

		// Retry up to 5 attempts
		MaxRetries: 5,
	}

	return elasticsearch.NewClient(cfg)

}

func recreateIndex() error {

	res, err = esClient.Indices.Delete([]string{indexName}, esClient.Indices.Delete.WithIgnoreUnavailable(true))
	if err != nil || res.IsError() {
		return err
	}
	res.Body.Close()
	log.Printf("→ Deleted %s index", indexName)

	res, err = esClient.Indices.Create(indexName)
	if err != nil {
		return err
	}
	res.Body.Close()

	log.Printf("→ Re-created %s index", indexName)
	return nil
}

func bulkIndexer() (esutil.BulkIndexer, error) {

	cfg := esutil.BulkIndexerConfig{
		Index:         indexName,        // The default index name
		Client:        esClient,         // The Elasticsearch client
		NumWorkers:    numWorkers,       // The number of worker goroutines
		FlushBytes:    int(flushBytes),  // The flush threshold in bytes
		FlushInterval: 30 * time.Second, // The periodic flush interval
	}

	return esutil.NewBulkIndexer(cfg)
}

func indexerItem(a *Article) esutil.BulkIndexerItem {

	// Prepare the data payload: encode article to JSON
	data, err := json.Marshal(a)
	if err != nil {
		log.Fatalf("Cannot encode article %d: %s", a.ID, err)
	}

	return esutil.BulkIndexerItem{
		// Action field configures the operation to perform (index, create, delete, update)
		Action: "index",

		// DocumentID is the (optional) document ID
		DocumentID: strconv.Itoa(a.ID),

		// Body is an `io.Reader` with the payload
		Body: bytes.NewReader(data),

		// OnSuccess is called for each successful operation
		OnSuccess: func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem) {
			atomic.AddUint64(&countSuccessful, 1)
		},

		// OnFailure is called for each failed operation
		OnFailure: func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem, err error) {
			if err != nil {
				log.Printf("ERROR: %s", err)
			} else {
				log.Printf("ERROR: %s: %s", res.Error.Type, res.Error.Reason)
			}
		},
	}
}

func printStats(bi esutil.BulkIndexer, since time.Time) {

	biStats := bi.Stats()

	// Report the results: number of indexed docs, number of errors, duration, indexing rate
	//
	log.Println(strings.Repeat("▔", 65))

	dur := time.Since(since)

	if biStats.NumFailed > 0 {
		log.Fatalf(
			"Indexed [%s] documents with [%s] errors in %s (%s docs/sec)",
			humanize.Comma(int64(biStats.NumFlushed)),
			humanize.Comma(int64(biStats.NumFailed)),
			dur.Truncate(time.Millisecond),
			humanize.Comma(int64(1000.0/float64(dur/time.Millisecond)*float64(biStats.NumFlushed))),
		)
	} else {
		log.Printf(
			"Sucessfuly indexed [%s] documents in %s (%s docs/sec)",
			humanize.Comma(int64(biStats.NumFlushed)),
			dur.Truncate(time.Millisecond),
			humanize.Comma(int64(1000.0/float64(dur/time.Millisecond)*float64(biStats.NumFlushed))),
		)
	}
}

// ParallelIndexer use the esutil.BulkIndexer
func ParallelIndexer() {

	log.SetFlags(0)

	log.Printf(
		"\x1b[1mParallelIndexer\x1b[0m: documents [%s] workers [%d] flush [%s]",
		humanize.Comma(int64(numItems)), numWorkers, humanize.Bytes(uint64(flushBytes)))

	log.Println(strings.Repeat("▁", 65))

	// Create Client
	esClient, err = newClient()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	// Create the BulkIndexer
	bi, err := bulkIndexer()
	if err != nil {
		log.Fatalf("Error creating the indexer: %s", err)
	}

	//
	articles := generateArticles()

	// Re-create the index
	err = recreateIndex()
	if err != nil {
		log.Fatalf("Cannot re-create index: %s", res)
	}

	start := time.Now().UTC()

	// Loop over the collection
	for _, a := range articles {

		// Add an item to the BulkIndexer
		err = bi.Add(
			context.Background(),
			indexerItem(a),
		)
		if err != nil {
			log.Fatalf("Unexpected error: %s", err)
		}
	}

	// Close the indexer
	if err := bi.Close(context.Background()); err != nil {
		log.Fatalf("Unexpected error: %s", err)
	}

	//
	printStats(bi, start)

}
