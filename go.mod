module bulk

go 1.14

require (
	github.com/cenkalti/backoff/v4 v4.0.0
	github.com/dustin/go-humanize v1.0.0
	github.com/elastic/go-elasticsearch/v7 v7.5.1-0.20200515132447-db2bcf51489c
)
